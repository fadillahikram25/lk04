import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            try {
                System.out.print("Silakan masukkan pembilang  :");
                int pembilang = scanner.nextInt();

                System.out.print("Silakan masukkan penyebut   :");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println(("Hasil pembagiannya adalah   :" + hasil));

                validInput = true;
            }
            catch(InputMismatchException e){
                System.out.println("Bilangan pembilang yang diinputkan harus berupa bilangan bulat. Silakan coba lagi");
                scanner.nextLine();
            }catch(ArithmeticException e){
                System.out.println("Bilangan penyebut tidak boleh bernilai 0. Silakan coba lagi");
                scanner.nextLine();
            }
            //edit here
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        if (penyebut == 0){
            throw new ArithmeticException("Bilangan penyebut tidak boleh bernilai 0");
        }
        //add exception apabila penyebut bernilai 0
        return pembilang / penyebut;
    }
}
